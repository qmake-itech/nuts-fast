package com.nuts.fast.common.data.enums;

import lombok.Getter;

/**
 * @author nuts
 */
@Getter
public enum DesensitizeMethodEnum {

    /**
     * 序列化脱敏
     */
    SERIALIZE,

    /**
     * 日志脱敏
     */
    LOG,
}

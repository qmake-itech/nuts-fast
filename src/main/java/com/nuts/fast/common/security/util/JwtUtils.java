package com.nuts.fast.common.security.util;

import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWT;
import cn.hutool.jwt.signers.JWTSigner;
import cn.hutool.jwt.signers.JWTSignerUtil;
import com.nuts.fast.common.core.security.NutsUserDetails;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Jwt 生成校验工具类
 *
 * @author Nuts
 */
@Component
public class JwtUtils {

    /**
     * 用于签名 Access Token
     */
    private final JWTSigner key;

    public JwtUtils() {
        key = JWTSignerUtil.none();
    }

    /**
     * 根据用户信息生成一个 JWT
     *
     * @param userDetails  用户信息
     * @return JWT
     */
    public String createJwtToken(NutsUserDetails userDetails) {
        return JWT.create()
                .setPayload("sub", userDetails.getUsername())
                .setPayload("userDetails", userDetails)
                .setPayload("authorities",
                        userDetails.getAuthorities().stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .setSigner(key)
                .sign();
    }

    public Optional<JSONObject> parsePayloads(String jwtToken) {
        return Optional.ofNullable(JWT.of(jwtToken).getPayloads());
    }
}

package com.nuts.fast.module.sys.service.intf;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.module.sys.model.entity.SysNoticeUserDO;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * 系统管理 - 通知用户关联信息服务接口类
 *
 * @author Nuts
 */
public interface ISysNoticeUserService extends IService<SysNoticeUserDO> {

    /**
     * 通过用户主键获取当前用户的通知列表
     *
     * @param uid 用户主键
     * @return 当前用户的通知列表
     */
    List<SysNoticeUserDO> getSysNoticeUserListByUid(Long uid);

    /**
     * 根据通知主键删除通知用户关联信息
     *
     * @param nid 通知主键
     * @return 成功标志
     */
    boolean removeByNid(Serializable nid);

    /**
     * 根据通知主键列表批量删除通知用户关联信息
     *
     * @param nids 通知主键列表
     * @return 成功标志
     */
    boolean removeByNids(Collection<?> nids);
}

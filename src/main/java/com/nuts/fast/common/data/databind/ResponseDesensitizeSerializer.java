package com.nuts.fast.common.data.databind;

import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.nuts.fast.common.data.annotation.Desensitize;
import com.nuts.fast.common.data.enums.DesensitizeMethodEnum;
import com.nuts.fast.common.data.enums.DesensitizeTypeEnum;
import com.nuts.fast.common.data.util.DesensitizeUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;

/**
 * 自定义脱敏序列化器(响应序列化脱敏)
 *
 * @author nuts
 */
@NoArgsConstructor
@AllArgsConstructor
public class ResponseDesensitizeSerializer extends JsonSerializer<String> implements ContextualSerializer {

    /**
     * 脱敏类型
     */
    private DesensitizeTypeEnum desensitizeTypeEnum;

    /**
     * 前置不脱敏位数
     */
    private Integer prefixNoMaskLength;

    /**
     * 后置不脱敏位数
     */
    private Integer suffixNoMaskLength;

    /**
     * 脱敏使用符号
     */
    private String symbol;

    /**
     * 序列化字符串
     *
     * @param value       原字符串
     * @param gen         生成 JSON 内容
     * @param serializers Provider that can be used to get serializers for
     *                    serializing Objects value contains, if any.
     */
    @Override
    public void serialize(final String value, final JsonGenerator gen,
                          final SerializerProvider serializers) throws IOException {

        if (StrUtil.isNotBlank(value) && desensitizeTypeEnum != null) {
            switch (desensitizeTypeEnum) {
                case CUSTOMER ->
                        gen.writeString(DesensitizeUtils.desValue(value, prefixNoMaskLength, suffixNoMaskLength, symbol));
                case PHONE -> gen.writeString(DesensitizeUtils.hidePhone(value));
                case EMAIL -> gen.writeString(DesensitizeUtils.hideEmail(value));
                case ID_CARD -> gen.writeString(DesensitizeUtils.hideIdCard(value));
                case NAME -> gen.writeString(DesensitizeUtils.hideChineseName(value));
                case PASSWORD -> gen.writeString(DesensitizeUtils.hidePassword(value));
                default -> throw new IllegalArgumentException("unknown privacy type enum" + desensitizeTypeEnum);
            }
        }
    }

    /**
     * 获取注解以及字段类型
     *
     * @param prov     Serializer provider to use for accessing config, other serializers
     * @param property Method or field that represents the property
     *                 (and is used to access value to serialize).
     *                 Should be available; but there may be cases where caller cannot provide it and
     *                 null is passed instead (in which case impls usually pass 'this' serializer as is)
     * @return Serializer to use for serializing values of specified property;
     * may be this instance or a new instance.
     * @throws JsonMappingException
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov,
                                              final BeanProperty property) throws JsonMappingException {

        if (property != null) {
            if (Objects.equals(property.getType().getRawClass(), String.class)) {

                Desensitize desensitize = property.getAnnotation(Desensitize.class);

                if (desensitize == null) {
                    desensitize = property.getContextAnnotation(Desensitize.class);
                }

                if (desensitize != null && Arrays.asList(desensitize.method()).contains(DesensitizeMethodEnum.SERIALIZE)) {
                    return new ResponseDesensitizeSerializer(
                            desensitize.type(),
                            desensitize.prefixNoMaskLength(),
                            desensitize.suffixNoMaskLength(),
                            desensitize.symbol()
                    );
                }
            }
            return prov.findValueSerializer(property.getType(), property);
        }
        return prov.findNullValueSerializer(null);
    }
}

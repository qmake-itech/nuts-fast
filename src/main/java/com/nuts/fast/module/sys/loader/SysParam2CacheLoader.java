package com.nuts.fast.module.sys.loader;

import com.nuts.fast.module.sys.service.intf.ISysParamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 系统参数启动时加载至缓存
 *
 * @author Nuts
 */
@Slf4j
@Component
@Order(2)
public class SysParam2CacheLoader implements CommandLineRunner {

    @Resource
    private ISysParamService sysParamService;

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     */
    @Override
    public void run(String... args) {

        sysParamService.getParamMap();
        log.debug("==================系统参数已加载==================");
    }
}

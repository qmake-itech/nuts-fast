package com.nuts.fast.module.auth.model.dto.menu;

import com.nuts.fast.module.auth.model.entity.AuthActionDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
public class AuthMenuShowDTO implements Serializable {

    /**
     * 菜单编号
     */
    @Schema(name = "code", description = "菜单编号")
    private String code;

    /**
     * 菜单图标
     */
    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    /**
     * 访问方法
     */
    @Schema(name = "scheme", description = "访问方法")
    private Integer scheme;

    /**
     * 访问方式
     */
    @Schema(name = "target", description = "访问方式")
    private Integer target;

    /**
     * 组件路径
     */
    @Schema(name = "path", description = "组件路径")
    private String path;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "是否在菜单中可见")
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "是否在面包屑中显示")
    private String showInBread;

    /**
     * 组件是否缓存
     */
    @Schema(name = "keepLive", description = "组件是否缓存")
    private String keepLive;

    /**
     * 操作列表
     */
    @Schema(name = "actions", description = "操作列表")
    private List<AuthActionDO> actions;
}

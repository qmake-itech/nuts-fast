package com.nuts.fast.common.web.method.annotation;

import cn.hutool.core.convert.Convert;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ValueConstants;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * @author Nuts
 */
@Slf4j
@Component
public class RequestBodyBySingleParamResolver extends AbstractNamedValueMethodArgumentResolver {

    /**
     * 获取注解的 name 值
     *
     * @param parameter the method parameter
     * @return the named value information
     */
    @NotNull
    @Override
    protected NamedValueInfo createNamedValueInfo(MethodParameter parameter) {

        // 获取 @RequestBodyBySingleParam 注解
        RequestBodyBySingleParam annotation = parameter.getParameterAnnotation(RequestBodyBySingleParam.class);

        if (annotation == null) {
            return new RequestBodyBySingleParamNamedValueInfo();
        } else {
            return new RequestBodyBySingleParamNamedValueInfo(annotation);
        }
    }

    /**
     * 方法注解校验, 判断是否包含 @RequestBodyBySingleParam 注解
     *
     * @param parameter the method parameter to check
     * @return {@code true} if this resolver supports the supplied parameter;
     * {@code false} otherwise
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(RequestBodyBySingleParam.class);
    }

    /**
     * Resolve the given parameter type and value name into an argument value.
     *
     * @param name      the name of the value being resolved
     * @param parameter the method parameter to resolve to an argument value
     *                  (pre-nested in case of a {@link Optional} declaration)
     * @param request   the current request
     * @return the resolved argument (may be {@code null})
     * @throws Exception in case of errors
     */
    @Override
    protected Object resolveName(@NotNull String name, @NotNull MethodParameter parameter, NativeWebRequest request) throws Exception {

        HttpServletRequest servletRequest = request.getNativeRequest(HttpServletRequest.class);

        String contentType = Objects.requireNonNull(servletRequest).getContentType();

        if (contentType == null || !contentType.contains(MediaType.APPLICATION_JSON_VALUE)) {
            throw new RuntimeException("解析参数异常，contentType需为" + MediaType.APPLICATION_JSON_VALUE);
        }

        if (!HttpMethod.POST.name().equalsIgnoreCase(servletRequest.getMethod())) {
            throw new RuntimeException("解析参数异常，请求类型必须为" + HttpMethod.POST.name());
        }

        String jsonStr = getRequestBody(servletRequest);

        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, Object> requestMap = objectMapper.readValue(jsonStr, Map.class);
        Type parameterType = parameter.getGenericParameterType();
        String parameterName = parameter.getParameterName();

        if (name.isEmpty()) {
            return Convert.convert(parameterType, requestMap.get(parameterName));
        } else {
            return Convert.convert(parameterType, requestMap.get(name));
        }
    }

    /**
     * 获取请求body
     *
     * @param servletRequest request
     * @return 请求body
     */
    private String getRequestBody(HttpServletRequest servletRequest) {

        StringBuilder stringBuilder = new StringBuilder();

        try {

            BufferedReader reader = servletRequest.getReader();
            char[] buf = new char[1024];
            int length;
            while ((length = reader.read(buf)) != -1) {
                stringBuilder.append(buf, 0, length);
            }
        } catch (IOException e) {

            log.error("读取流异常", e);
            throw new RuntimeException("读取流异常");
        }

        return stringBuilder.toString();
    }

    private static class RequestBodyBySingleParamNamedValueInfo extends NamedValueInfo {

        public RequestBodyBySingleParamNamedValueInfo() {
            super("", false, ValueConstants.DEFAULT_NONE);
        }

        public RequestBodyBySingleParamNamedValueInfo(RequestBodyBySingleParam annotation) {
            super(annotation.name(), annotation.required(), annotation.defaultValue());
        }
    }
}

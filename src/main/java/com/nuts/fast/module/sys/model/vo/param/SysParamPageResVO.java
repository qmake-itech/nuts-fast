package com.nuts.fast.module.sys.model.vo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 系统参数分页信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamPageResVO", description = "系统管理 - 系统参数分页信息返回 VO")
public class SysParamPageResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private String id;

    /**
     * 参数名称
     */
    @Schema(name = "paramName", description = "参数名称")
    private String paramName;

    /**
     * 参数键
     */
    @Schema(name = "paramKey", description = "参数键")
    private String paramKey;

    /**
     * 参数值
     */
    @Schema(name = "paramValue", description = "参数值")
    private String paramValue;

    /**
     * 参数类型
     */
    @Schema(name = "paramType", description = "参数类型")
    private String paramType;

    /**
     * 是否系统内置
     */
    @Schema(name = "isBuildIn", description = "是否系统内置")
    private String isBuildIn;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdByName", description = "创建人")
    private String createdByName;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedByName", description = "更新人")
    private String updatedByName;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}

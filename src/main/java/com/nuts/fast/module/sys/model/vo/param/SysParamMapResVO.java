package com.nuts.fast.module.sys.model.vo.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Map;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamMapResVO", description = "系统管理 - 参数缓存信息返回 VO")
public class SysParamMapResVO implements Serializable {

    /**
     * 字典名称
     */
    @Schema(name = "paramMap", description = "参数集合")
    private Map<String, String> paramMap;
}

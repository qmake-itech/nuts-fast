package com.nuts.fast.module.sys.model.vo.notice;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 通知公告保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysNoticeSaveReqVO", description = "系统管理 - 通知公告保存请求 VO")
public class SysNoticeSaveReqVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 通知标题
     */
    @Schema(name = "noticeTitle", description = "通知标题")
    private String noticeTitle;

    /**
     * 通知内容
     */
    @Schema(name = "noticeContent", description = "通知内容")
    private String noticeContent;

    /**
     * 通知推送时间
     */
    @Schema(name = "noticeTime", description = "通知推送时间")
    private Date noticeTime;

    /**
     * 是否定时自动推送
     * Y - 是 N - 否
     */
    @Schema(name = "autoNotice", description = "是否定时自动推送；Y - 是 N - 否")
    private String autoNotice;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}

package com.nuts.fast.module.sys.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 字典信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "系统管理 - 字典信息", description = "系统管理 - 字典信息")
@TableName("sys_dict")
public class SysDictDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 字典名称
     */
    @Schema(name = "dictName", description = "字典名称")
    private String dictName;

    /**
     * 字典描述
     */
    @Schema(name = "dictDesc", description = "字典描述")
    private String dictDesc;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdBy", description = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedBy", description = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}

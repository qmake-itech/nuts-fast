package com.nuts.fast.module.auth.model.dto.role;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 角色管理 - 角色所属用户列表信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthRoleUserDTO", description = "角色管理 - 角色所属用户列表信息返回 VO")
public class AuthRoleUserDTO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private String id;

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名")
    private String username;

    /**
     * 昵称
     */
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}

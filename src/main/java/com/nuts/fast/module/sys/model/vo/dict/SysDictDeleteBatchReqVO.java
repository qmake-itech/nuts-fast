package com.nuts.fast.module.sys.model.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictDeleteBatchReqVO", description = "系统管理 - 字典批量删除请求 VO")
public class SysDictDeleteBatchReqVO implements Serializable {

    /**
     * 主键列表
     */
    @Schema(name = "ids", description = "主键列表")
    private List<Long> ids;
}

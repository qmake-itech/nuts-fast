package com.nuts.fast.common.file.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 项目文件相关配置
 *
 * @author nuts
 */
@Configuration
@ConfigurationProperties(prefix = "nuts.file")
public class FileProperties {

    @Getter
    @Setter
    private Minio minio = new Minio();

    /**
     * Minio 分布式文件系统相关配置
     */
    @Getter
    @Setter
    public static class Minio {

        /**
         * 外网访问服务地址
         */
        private String url;

        /**
         * 用户名
         */
        private String accessKey;

        /**
         * 密码
         */
        private String secretKey;
        /**
         * 桶名称
         */
        private String bucketName;
    }
}

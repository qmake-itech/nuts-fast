package com.nuts.fast.module.sys.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.redis.constant.RedisConstants;
import com.nuts.fast.module.sys.mapper.SysDictMapper;
import com.nuts.fast.module.sys.model.convert.SysDictConvert;
import com.nuts.fast.module.sys.model.entity.SysDictDO;
import com.nuts.fast.module.sys.model.entity.SysDictItemDO;
import com.nuts.fast.module.sys.model.vo.dict.*;
import com.nuts.fast.module.sys.service.intf.ISysDictItemService;
import com.nuts.fast.module.sys.service.intf.ISysDictService;
import lombok.val;
import org.redisson.api.RBucket;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * 系统管理 - 字典信息服务实现类
 *
 * @author Nuts
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDictDO> implements ISysDictService {

    /**
     * 默认字典缓存时间 24h
     */
    private static final long TIME_STEP = 60 * 60 * 24L;

    @Resource
    private ISysDictItemService sysDictItemService;

    @Resource
    private RedissonClient redissonClient;

    /**
     * 获取字典列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 字典列表分页信息
     */
    @Override
    public IPage<SysDictPageResVO> page(PageModel<SysDictPageReqVO> pageParams) {
        return this.baseMapper.getSysDictListByPage(new Page<>(pageParams.getCurrent(), pageParams.getSize()), pageParams.getParams());
    }

    /**
     * 通过字典主键获取字典详情信息
     *
     * @param id 字典主键
     * @return 字典详情信息
     */
    @Override
    public SysDictInfoResVO getInfoById(Long id) {

        val dictItems = sysDictItemService.lambdaQuery().eq(SysDictItemDO::getDid, id).list();

        return this.baseMapper.getSysDictInfoById(id).withDictItems(dictItems);
    }

    /**
     * 保存字典信息
     *
     * @param sysDictSaveReq 字典保存请求信息
     * @return 字典主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveOrUpdate(SysDictSaveReqVO sysDictSaveReq) {

        SysDictDO dict;

        if (ObjectUtil.isEmpty(sysDictSaveReq.getId())) {
            dict = insertSysDict(sysDictSaveReq);
        } else {
            dict = updateSysDict(sysDictSaveReq);
        }

        this.saveOrUpdate(dict);

        val id = dict.getId();

        // 新增字典项列表
        val insertDictItems = sysDictSaveReq.getInsertDictItems();
        if (insertDictItems != null && insertDictItems.size() != 0) {
            insertDictItems.forEach(sysDictItem -> sysDictItemService.save(sysDictItem.withDid(id)));
        }

        // 修改字典项列表
        val updateDictItems = sysDictSaveReq.getUpdateDictItems();
        if (updateDictItems != null && updateDictItems.size() != 0) {
            updateDictItems.forEach(sysDictItem -> sysDictItemService.updateById(sysDictItem.withDid(id)));
        }

        // 删除字典项列表
        val removeDictItems = sysDictSaveReq.getRemoveDictItems();
        if (removeDictItems != null && removeDictItems.size() != 0) {
            removeDictItems.forEach(sysDictItem -> sysDictItemService.removeById(sysDictItem));
        }

        return id;
    }

    /**
     * 根据字典主键删除字典信息
     *
     * @param id 字典主键
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeById(Serializable id) {
        return super.removeById(id) && sysDictItemService.removeByDid(id);
    }

    /**
     * 根据字典主键列表批量删除字典信息
     *
     * @param ids 字典主键列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean removeByIds(Collection<?> ids) {
        return super.removeByIds(ids) && sysDictItemService.removeByDids(ids);
    }

    /**
     * 从数据库中加载字典缓存
     *
     * @return 字典缓存
     */
    @Override
    public Map<String, List<SysDictItemResVO>> updateDictMapCache() {

        RMapCache<String, Map<String, List<SysDictItemResVO>>> cache = redissonClient.getMapCache(RedisConstants.DICT_MAP);
        RBucket<String> rBucket = redissonClient.getBucket(RedisConstants.DICT_MD5);

        Map<String, List<SysDictItemResVO>> dictMap = new HashMap<>(8);

        this.baseMapper.getSysDictList().forEach(sysDictWithDictItem -> {
            val dictName = sysDictWithDictItem.getDictName();
            List<SysDictItemResVO> sysDictItemResList = dictMap.get(dictName);
            if (sysDictItemResList == null) {
                dictMap.put(dictName, new ArrayList<>(List.of(SysDictItemResVO.builder()
                        .dictItemKey(sysDictWithDictItem.getDictItemKey())
                        .dictItemValue(sysDictWithDictItem.getDictItemValue())
                        .tagType(sysDictWithDictItem.getTagType())
                        .build())));
            } else {
                sysDictItemResList.add(SysDictItemResVO.builder()
                        .dictItemKey(sysDictWithDictItem.getDictItemKey())
                        .dictItemValue(sysDictWithDictItem.getDictItemValue())
                        .tagType(sysDictWithDictItem.getTagType())
                        .build());
                dictMap.put(dictName, sysDictItemResList);
            }
        });

        // 将字典缓存进 Redisson
        cache.put("dict_map", dictMap, TIME_STEP, TimeUnit.SECONDS);
        // 将字典 MD5 的值存入缓存，用于版本校验
        rBucket.set(SecureUtil.md5(dictMap.toString()), TIME_STEP, TimeUnit.SECONDS);

        return dictMap;
    }

    /**
     * 获取所有生效的字典下字典项
     *
     * @return 字典及字典项列表
     */
    @Override
    public Map<String, List<SysDictItemResVO>> getDictWithDictItemList() {

        RMapCache<String, Map<String, List<SysDictItemResVO>>> cache = redissonClient.getMapCache(RedisConstants.DICT_MAP);

        Map<String, List<SysDictItemResVO>> cacheDictMap = cache.get("dict_map");

        if (cacheDictMap == null || cacheDictMap.isEmpty()) {
            log.debug("加载字典 - 数据库");
            cacheDictMap = updateDictMapCache();
        } else {
            log.debug("加载字典 - redis");
        }

        return cacheDictMap;
    }

    /**
     * 新增字典信息
     *
     * @param sysDictSaveReq 字典保存请求信息
     * @return 字典主键
     */
    private SysDictDO insertSysDict(SysDictSaveReqVO sysDictSaveReq) {

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        return SysDictConvert.INSTANCE
                .saveReqVoConvert2Do(sysDictSaveReq)
                .withCreatedBy(currentUserId)
                .withCreatedTime(new Date())
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());
    }

    /**
     * 修改字典信息
     *
     * @param sysDictSaveReq 字典保存请求信息
     */
    private SysDictDO updateSysDict(SysDictSaveReqVO sysDictSaveReq) {

        val oldDict = this.baseMapper.selectById(sysDictSaveReq.getId());

        val currentUserId = SecurityUtils.getUser()
                .orElseThrow().getUserId();

        return SysDictConvert.INSTANCE
                .saveReqVoConvert2Do(oldDict, sysDictSaveReq)
                .withUpdatedBy(currentUserId)
                .withUpdatedTime(new Date());
    }
}

package com.nuts.fast.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.module.sys.model.dto.dict.SysDictWithDictItemDTO;
import com.nuts.fast.module.sys.model.entity.SysDictDO;
import com.nuts.fast.module.sys.model.vo.dict.SysDictInfoResVO;
import com.nuts.fast.module.sys.model.vo.dict.SysDictPageReqVO;
import com.nuts.fast.module.sys.model.vo.dict.SysDictPageResVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统管理 - 字典信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDictDO> {

    /**
     * 获取字典分页信息
     *
     * @param page           分页查询
     * @param sysDictPageReq 字典分页信息请求体
     * @return 字典分页信息
     */
    IPage<SysDictPageResVO> getSysDictListByPage(Page<SysDictPageResVO> page, @Param("sysDictPageReq") SysDictPageReqVO sysDictPageReq);

    /**
     * 根据字典主键获取字典详情信息
     *
     * @param id 字典主键
     * @return 字典详情信息
     */
    SysDictInfoResVO getSysDictInfoById(Long id);

    /**
     * 获取所有生效的字典下字典项
     *
     * @return 字典及字典项列表
     */
    List<SysDictWithDictItemDTO> getSysDictList();
}

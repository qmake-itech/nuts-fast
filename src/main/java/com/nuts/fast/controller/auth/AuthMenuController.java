package com.nuts.fast.controller.auth;

import cn.hutool.core.lang.tree.Tree;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuInfoResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListReqVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuListResVO;
import com.nuts.fast.module.auth.model.vo.menu.AuthMenuSaveReqVO;
import com.nuts.fast.module.auth.service.intf.IAuthMenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 菜单信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/auth/menu")
@Tag(name = "菜单信息服务接口", description = "菜单信息服务接口")
public class AuthMenuController {

    @Resource
    private IAuthMenuService authMenuService;

    /**
     * 获取全部菜单树
     *
     * @return 菜单树
     */
    @GetMapping("/tree/all")
    @Operation(summary = "获取全部菜单树", description = "获取全部菜单树, 组装给菜单详情页面父菜单输入项使用")
    public List<Tree<Long>> getAllAuthMenuTree() {
        return authMenuService.getAllAuthMenuTree();
    }

    /**
     * 获取全部菜单树(包含操作)
     *
     * @return 菜单树(包含操作)
     */
    @GetMapping("/tree/action/all")
    @Operation(summary = "获取全部菜单树(包含操作)", description = "获取全部菜单树(包含操作), 组装给前端选择权限使用")
    public List<Tree<Long>> getAllAuthMenuWithActionTree() {
        return authMenuService.getAllAuthMenuWithActionTree();
    }

    /**
     * 获取菜单列表信息
     *
     * @param authMenuListReq 查询参数
     * @return 菜单列表分页信息
     */
    @GetMapping("/list")
    @Operation(summary = "获取菜单列表信息", description = "获取菜单列表信息")
    public List<AuthMenuListResVO> getAuthMenuList(AuthMenuListReqVO authMenuListReq) {
        return authMenuService.getAuthMenuList(authMenuListReq);
    }

    /**
     * 获取菜单详情信息
     *
     * @param id 菜单 Id
     * @return 菜单详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取菜单详情信息", description = "获取菜单详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "菜单主键"))
    public AuthMenuInfoResVO getAuthMenuInfo(@Valid @NotNull Long id) {
        return authMenuService.getAuthMenuInfoById(id);
    }

    /**
     * 保存菜单信息
     *
     * @param authMenuSaveReq 菜单保存请求信息
     * @return 菜单主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存菜单信息", description = "保存菜单信息")
    public Long saveAuthMenu(@Valid @RequestBody AuthMenuSaveReqVO authMenuSaveReq) {
        return authMenuService.saveAuthMenu(authMenuSaveReq);
    }

    /**
     * 根据菜单主键删除菜单信息
     *
     * @param id 菜单主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据菜单主键删除菜单信息", description = "根据菜单主键删除菜单信息")
    @Parameters(@Parameter(name = "id", required = true, description = "菜单主键"))
    public void deleteAuthMenuById(@Valid @NotNull Long id) {
        authMenuService.deleteAuthMenuById(id);
    }

    /**
     * 根据菜单主键列表批量删除菜单信息
     *
     * @param ids 菜单主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据菜单主键列表批量删除菜单信息", description = "根据菜单主键列表批量删除菜单信息")
    @Parameters(@Parameter(name = "id", required = true, description = "菜单主键"))
    public void deleteAuthMenuById(@RequestBodyBySingleParam List<Long> ids) {
        authMenuService.deleteAuthMenuByIds(ids);
    }
}

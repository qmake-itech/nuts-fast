package com.nuts.fast.module.sys.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统管理 - 系统参数信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamDO", description = "系统管理 - 系统参数信息实体类")
@TableName("sys_param")
public class SysParamDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;
    
    /**
     * 参数名称
     */
    @Schema(name = "paramName", description = "参数名称")
    private String paramName;
    
    /**
     * 参数键
     */
    @Schema(name = "paramKey", description = "参数键")
    private String paramKey;
    
    /**
     * 参数值
     */
    @Schema(name = "paramValue", description = "参数值")
    private String paramValue;

    /**
     * 参数类型
     */
    @Schema(name = "paramType", description = "参数类型")
    private String paramType;
    
    /**
     * 是否系统内置
     */
    @Schema(name = "isBuildIn", description = "是否系统内置")
    private String isBuildIn;
    
    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
    
    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
    
    /**
     * 创建人
     */
    @Schema(name = "createdBy", description = "创建人")
    private Long createdBy;
    
    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;
    
    /**
     * 更新人
     */
    @Schema(name = "updatedBy", description = "更新人")
    private Long updatedBy;
    
    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}

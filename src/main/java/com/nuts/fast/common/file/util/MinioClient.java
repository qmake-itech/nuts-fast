package com.nuts.fast.common.file.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.util.IdUtil;
import com.nuts.fast.common.core.util.SpringUtils;
import com.nuts.fast.common.file.config.FileProperties;
import io.minio.*;
import io.minio.messages.Bucket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Minio 文件上传客户端
 *
 * @author nuts
 */
@Slf4j
public class MinioClient {

    /**
     * 文件配置参数
     */
    private static final FileProperties FILE_PROPERTIES = SpringUtils.getBean(FileProperties.class);

    /**
     * minio 内置客户端
     */
    private static final io.minio.MinioClient MINIO_CLIENT = SpringUtils.getBean(io.minio.MinioClient.class);


    /**
     * 创建桶
     *
     * @param bucketName 桶名称
     */

    public static void makeBucket(String bucketName) throws Exception {
        if (!MINIO_CLIENT.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())) {
            MINIO_CLIENT.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
        }
    }

    /**
     * 获取全部bucket
     * <p>
     * <a href="https://docs.minio.io/cn/java-client-api-reference.html#listBuckets">
     * https://docs.minio.io/cn/java-client-api-reference.html#listBuckets
     * </a>
     */
    public static List<Bucket> listBuckets() throws Exception {
        return MINIO_CLIENT.listBuckets();
    }

    /**
     * 根据 bucketName 获取信息
     *
     * @param bucketName bucket名称
     */

    public static Optional<Bucket> getBucket(String bucketName) throws Exception {
        return listBuckets().stream().filter(b -> b.name().equals(bucketName)).findFirst();
    }

    /**
     * 根据 bucketName 删除信息
     *
     * @param bucketName bucket名称
     */

    public static void removeBucket(String bucketName) throws Exception {
        MINIO_CLIENT.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
    }

    /**
     * 获取文件外链
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @return url
     */
    public static String getObjectURL(String bucketName, String objectName) throws Exception {
        return getObjectURL(bucketName, objectName, 7);
    }

    /**
     * 获取文件外链
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @param expires    过期时间 <=7
     * @return url
     */
    public static String getObjectURL(String bucketName, String objectName, Integer expires) throws Exception {
        return MINIO_CLIENT.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder().bucket(bucketName).object(objectName).expiry(expires).build());
    }

    /**
     * 获取文件访问地址(有效期)
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @return url
     */
    public static String getPresignedObjectUrl(String bucketName, String objectName) throws Exception {
        return getPresignedObjectUrl(bucketName, objectName, null);
    }

    /**
     * 获取文件访问地址(有效期)
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @param expires    过期时间 <=7
     * @return url
     */
    public static String getPresignedObjectUrl(String bucketName, String objectName, Integer expires) throws Exception {
        return MINIO_CLIENT.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder().bucket(bucketName).object(objectName).build());
    }

    /**
     * 获取文件
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @return 二进制流
     */
    public static InputStream getObject(String bucketName, String objectName) throws Exception {
        return MINIO_CLIENT.getObject(GetObjectArgs.builder().bucket(bucketName).object(objectName).build());
    }

    /**
     * 获取文件
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     * @return 二进制流
     */
    public static InputStream getObject(String bucketName, String objectName, long offset, Long length) throws Exception {
        return MINIO_CLIENT.getObject(GetObjectArgs.builder().bucket(bucketName).object(objectName).offset(offset).length(length).build());
    }

    public static String putObject(String bucketName, String objectName, MultipartFile file) throws Exception {

        if (objectName == null || "".equals(objectName)) {
            objectName = extractFilename(file);
        }

        PutObjectArgs args = PutObjectArgs.builder()
                .bucket(bucketName)
                .object(objectName)
                .stream(file.getInputStream(), file.getSize(), -1)
                .contentType(file.getContentType())
                .build();
        makeBucket(bucketName);
        MINIO_CLIENT.putObject(args);
        return FILE_PROPERTIES.getMinio().getUrl() + "/" + bucketName + "/" + objectName;
    }

    public static String putObject(String bucketName, String objectName, File file) throws Exception {

        if (objectName == null || "".equals(objectName)) {
            objectName = extractFilename(file);
        }

        try (InputStream inputStream = new FileInputStream(file)) {
            PutObjectArgs args = PutObjectArgs.builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .stream(inputStream, file.length(), -1)
                    .contentType(FileTypeUtil.getType(file))
                    .build();
            makeBucket(bucketName);
            MINIO_CLIENT.putObject(args);
        }
        return FILE_PROPERTIES.getMinio().getUrl() + "/" + bucketName + "/" + objectName;
    }

    public static String putObject(String bucketName, MultipartFile file) throws Exception {
        return putObject(bucketName, null, file);
    }

    public static String putObject(String bucketName, File file) throws Exception {
        return putObject(bucketName, null, file);
    }

    /**
     * 删除文件
     * <a href="https://docs.minio.io/cn/java-client-api-reference.html#removeObject">
     * https://docs.minio.io/cn/java-client-api-reference.html#removeObject
     * </a>
     *
     * @param bucketName bucket名称
     * @param objectName 文件名称
     */
    public static void removeObject(String bucketName, String objectName) throws Exception {
        MINIO_CLIENT.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
    }

    /**
     * 编码文件名
     *
     * @param file
     * @return
     */
    public static String extractFilename(MultipartFile file) {

        String filenameWithExtension = file.getOriginalFilename();
        return DateUtil.format(new Date(), "yyyy/MM/dd") + "/" + IdUtil.fastUUID() + "/" + filenameWithExtension;
    }

    /**
     * 编码文件名
     *
     * @param file
     * @return
     */
    public static String extractFilename(File file) {

        String filename = FileNameUtil.mainName(file);
        String extension = FileNameUtil.extName(file);
        return DateUtil.format(new Date(), "yyyy/MM/dd") + "/" + IdUtil.fastUUID() + "/" + filename + "." + extension;
    }
}

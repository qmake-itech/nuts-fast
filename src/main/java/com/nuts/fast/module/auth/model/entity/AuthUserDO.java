package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限管理 - 用户信息表
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "权限管理-用户信息", description = "权限管理-用户信息")
@TableName("auth_user")
public class AuthUserDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名")
    private String username;

    /**
     * 密码(加密后)
     */
    @Schema(name = "password", description = "密码(加密后)")
    private String password;

    /**
     * 昵称
     */
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    /**
     * 性别
     */
    @Schema(name = "gender", description = "性别")
    private Integer gender;

    /**
     * 手机号
     */
    @Schema(name = "mobile", description = "手机号")
    private String mobile;

    /**
     * 电子邮箱
     */
    @Schema(name = "email", description = "电子邮箱")
    private String email;

    /**
     * 错误次数
     */
    @Schema(name = "errorTimes", description = "错误次数")
    private Integer errorTimes;

    /**
     * 上次锁定时间
     */
    @Schema(name = "lockedTime", description = "上次锁定时间")
    private Date lockedTime;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdBy", description = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedBy", description = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}

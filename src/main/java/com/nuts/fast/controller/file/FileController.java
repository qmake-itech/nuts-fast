package com.nuts.fast.controller.file;

import com.nuts.fast.common.file.config.FileProperties;
import com.nuts.fast.common.file.util.MinioClient;
import com.nuts.fast.common.file.util.OssFile;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 文件服务展示接口
 *
 * @author nuts
 */
@Slf4j
@RestController
@RequestMapping("/file")
@Tag(name = "文件服务接口", description = "文件信息服务接口")
public class FileController {

    @Resource
    private FileProperties fileProperties;

    /**
     * 文件上传
     *
     * @param file 上传的文件
     * @return 访问地址
     */
    @PostMapping("/upload")
    public OssFile uploadFile(MultipartFile file) throws Exception {

        String fileKey = MinioClient.extractFilename(file);
        String bucketName = fileProperties.getMinio().getBucketName();
        String url = MinioClient.putObject(bucketName, null, file);

        return OssFile.builder()
                .fileName(file.getOriginalFilename())
                .fileLength(file.getSize())
                .fileExtension(file.getContentType())
                .url(url)
                .fileKey(fileKey)
                .fileDir(bucketName)
                .build();
    }
}

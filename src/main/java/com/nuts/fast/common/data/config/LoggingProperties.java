package com.nuts.fast.common.data.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;

/**
 * 项目日志相关配置
 *
 * @author nuts
 */
@Validated
@Configuration
@ConfigurationProperties(prefix = "nuts.logging")
public class LoggingProperties {

    /**
     * 日志文件存放目录
     */
    @Getter
    @Setter
    private String filePath = "./logs/";

    /**
     * 日志文件名前缀
     */
    @Getter
    @Setter
    private String fileName = "application";

    @Getter
    @Setter
    @Valid
    private Desensitize desensitize = new Desensitize();

    /**
     * 脱敏相关配置
     */
    @Getter
    @Setter
    public static class Desensitize {

        /**
         * 日志脱敏默认处理包
         */
        private String packageScanner = "com.nuts.fast";
    }
}

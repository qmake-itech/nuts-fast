package com.nuts.fast.common.core.security;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.val;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

/**
 * @author Nuts
 */
public class SecurityUtils {

    public static Optional<NutsUserDetails> getUser() {

        val authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.isAuthenticated()) {

            if (authentication.getPrincipal() instanceof NutsUserDetails) {
                return Optional.ofNullable((NutsUserDetails) authentication.getPrincipal());
            }

            if (authentication.getPrincipal() instanceof JSONObject) {
                return Optional.ofNullable(JSONUtil.toBean((JSONObject) authentication.getPrincipal(), NutsUserDetails.class));
            }
        }

        return Optional.empty();
    }
}

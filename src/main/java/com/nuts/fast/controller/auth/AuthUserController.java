package com.nuts.fast.controller.auth;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.auth.model.vo.user.AuthUserInfoResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageReqVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserPageResVO;
import com.nuts.fast.module.auth.model.vo.user.AuthUserSaveReqVO;
import com.nuts.fast.module.auth.service.intf.IAuthRoleService;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/auth/user")
@Tag(name = "用户信息服务接口", description = "用户信息服务接口")
public class AuthUserController {

    @Resource
    private IAuthUserService authUserService;

    @Resource
    private IAuthRoleService authRoleService;

    /**
     * 获取用户分页列表
     *
     * @param pageParams 分页查询请求
     * @return 用户分页列表
     */
    @PostMapping("/list/page")
    @Operation(summary = "获取用户分页列表", description = "获取用户分页列表")
    public IPage<AuthUserPageResVO> getAuthUserListByPage(@RequestBody PageModel<AuthUserPageReqVO> pageParams) {
        return authUserService.page(pageParams);
    }


    /**
     * 获取用户详情信息
     *
     * @param id 用户 Id
     * @return 用户详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取用户详情信息", description = "获取用户详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "用户主键"))
    public AuthUserInfoResVO getAuthUserInfo(@Valid @NotNull Long id) {

        val roles = authRoleService.getRoleListByUserId(id);

        return authUserService.getInfoById(id).withRoles(roles);
    }

    /**
     * 保存用户信息
     *
     * @param authUserSaveReq 用户保存请求信息
     * @return 用户主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存用户信息", description = "保存用户信息")
    public Long saveAuthUser(@Valid @RequestBody AuthUserSaveReqVO authUserSaveReq) {
        return authUserService.saveOrUpdate(authUserSaveReq);
    }

    /**
     * 根据用户主键删除用户信息
     *
     * @param id 用户主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据用户主键删除用户信息", description = "根据用户主键删除用户信息")
    @Parameters(@Parameter(name = "id", required = true, description = "用户主键"))
    public void deleteAuthUserById(@Valid @NotNull Long id) {
        authUserService.removeById(id);
    }

    /**
     * 根据用户主键列表批量删除用户信息
     *
     * @param ids 用户主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据用户主键列表批量删除用户信息", description = "根据用户主键列表批量删除用户信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "用户主键列表"))
    public void deleteAuthUserById(@RequestBodyBySingleParam List<Long> ids) {
        authUserService.removeByIds(ids);
    }
}

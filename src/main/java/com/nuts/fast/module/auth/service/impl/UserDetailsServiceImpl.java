package com.nuts.fast.module.auth.service.impl;

import com.nuts.fast.common.core.exception.SystemException;
import com.nuts.fast.common.core.security.NutsUserDetails;
import com.nuts.fast.common.security.util.JwtUtils;
import com.nuts.fast.common.security.util.TokenUtils;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import com.nuts.fast.module.auth.service.intf.IUserDetailService;
import lombok.val;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * 权限管理 - 用户详情信息服务实现类 (Spring Security)
 *
 * @author Nuts
 */
@Service
public class UserDetailsServiceImpl implements IUserDetailService {

    @Resource
    private TokenUtils tokenUtils;

    @Resource
    private IAuthUserService authUserService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Resource
    private JwtUtils jwtUtils;

    /**
     * 通过用户名获取用户信息组装成 Spring Security 用户详情实体类
     *
     * @param username 用户名
     * @return Spring Security 用户详情实体类
     */
    @Override
    public NutsUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return authUserService.getNutsUserDetails(username);
    }

    /**
     * 系统登录
     *
     * @param username 用户名
     * @param password 密码
     * @return 登陆 token 信息
     */
    @Override
    public Optional<String> login(String username, String password) {
        return Optional.of(this.loadUserByUsername(username))
                .map(userDetails -> {

                    if (!passwordEncoder.matches(password, userDetails.getPassword())) {
                        throw new SystemException("用户名或密码错误!");
                    }


                    if (!userDetails.isEnabled()) {
                        throw new SystemException("该用户未激活!");
                    }

                    if (!userDetails.isAccountNonLocked()) {
                        throw new SystemException("该用户已被锁定!");
                    }

                    val jwtToken = jwtUtils.createJwtToken(userDetails);

                    return Optional.ofNullable(tokenUtils.cacheJwtToken(jwtToken));
                })
                .orElseThrow(() -> new SystemException("当前用户不存在!"));
    }

    /**
     * 系统登出
     *
     * @param token 登陆 token 信息
     */
    @Override
    public void logout(String token) {
        tokenUtils.removeJwtToken(token);
    }
}

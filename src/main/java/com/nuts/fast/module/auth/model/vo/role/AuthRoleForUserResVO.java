package com.nuts.fast.module.auth.model.vo.role;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 权限管理 - 用户管理页面获取全部用户请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthRoleForUserResVO", description = "权限管理 - 用户管理页面获取全部用户请求 VO")
public class AuthRoleForUserResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;


    /**
     * 角色名称
     */
    @Schema(name = "roleName", description = "角色名称")
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}

package com.nuts.fast.module.sys.service.intf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.sys.model.entity.SysParamDO;
import com.nuts.fast.module.sys.model.vo.param.*;

import java.util.Map;

/**
 * 系统管理 - 系统参数信息服务接口类
 *
 * @author Nuts
 */
public interface ISysParamService extends IService<SysParamDO> {

    /**
     * 获取系统参数列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 系统参数列表分页信息
     */
    IPage<SysParamPageResVO> page(PageModel<SysParamPageReqVO> pageParams);

    /**
     * 通过系统参数主键获取系统参数详情信息
     *
     * @param id 系统参数主键
     * @return 系统参数详情信息
     */
    SysParamInfoResVO getInfoById(Long id);

    /**
     * 保存系统参数信息
     *
     * @param sysParamSaveReq 系统参数保存请求信息
     * @return 系统参数主键
     */
    Long saveOrUpdate(SysParamSaveReqVO sysParamSaveReq);

    /**
     * 从数据库中加载参数缓存
     *
     * @return 参数缓存
     */
    Map<String, String> updateParamMapCache();

    /**
     * 获取所有生效的参数
     *
     * @return 参数列表
     */
    Map<String, String> getParamMap();
}

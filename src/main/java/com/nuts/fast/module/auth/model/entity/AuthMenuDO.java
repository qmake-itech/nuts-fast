package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * 权限管理 - 菜单信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthMenuDO", description = "权限管理 - 菜单信息, 前端菜单信息, 协助前端动态生成菜单")
@TableName("auth_menu")
public class AuthMenuDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 父菜单主键
     */
    @Schema(name = "pid", description = "父菜单主键")
    private Long pid;

    /**
     * 菜单编号
     */
    @Schema(name = "menuCode", description = "菜单编号")
    private String menuCode;

    /**
     * 菜单名称
     */
    @Schema(name = "menuName", description = "菜单名称")
    private String menuName;

    /**
     * 菜单图标
     */
    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    /**
     * 访问方法
     */
    @Schema(name = "scheme", description = "访问方法")
    private Integer scheme;

    /**
     * 访问方式
     */
    @Schema(name = "target", description = "访问方式")
    private Integer target;

    /**
     * 组件路径
     */
    @Schema(name = "path", description = "组件路径")
    private String path;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "是否在菜单中可见")
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "是否在面包屑中显示")
    private String showInBread;

    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序")
    private Integer sort;

    /**
     * 组件是否缓存
     */
    @Schema(name = "keepLive", description = "组件是否缓存")
    private String keepLive;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "createdBy", description = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Schema(name = "createdTime", description = "创建时间")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "updatedBy", description = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @Schema(name = "updatedTime", description = "更新时间")
    private Date updatedTime;
}

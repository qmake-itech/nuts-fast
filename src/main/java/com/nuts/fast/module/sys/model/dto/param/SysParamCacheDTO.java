package com.nuts.fast.module.sys.model.dto.param;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

/**
 * 系统管理 - 系统参数缓存信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysParamCacheDTO", description = "系统管理 - 系统参数缓存信息返回 DTO")
public class SysParamCacheDTO {

    /**
     * 参数键
     */
    @Schema(name = "paramKey", description = "参数键")
    private String paramKey;

    /**
     * 参数值
     */
    @Schema(name = "paramValue", description = "参数值")
    private String paramValue;

    /**
     * 参数类型
     */
    @Schema(name = "paramType", description = "参数类型")
    private String paramType;
}

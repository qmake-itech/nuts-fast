package com.nuts.fast.module.auth.model.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 用户管理 - 用户所属角色列表信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthUserRoleDTO", description = "用户管理 - 用户所属角色列表信息返回 VO")
public class AuthUserRoleDTO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private String id;

    /**
     * 角色编号
     */
    @Schema(name = "roleCode", description = "角色编号")
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(name = "roleName", description = "角色名称")
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}

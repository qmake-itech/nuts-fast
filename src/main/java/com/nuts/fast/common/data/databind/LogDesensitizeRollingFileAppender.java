package com.nuts.fast.common.data.databind;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.rolling.RollingFileAppender;
import com.nuts.fast.common.data.util.DesensitizeUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author nuts
 */
@Slf4j
public class LogDesensitizeRollingFileAppender extends RollingFileAppender {

    /**
     * 日志相关配置信息
     */
    @Setter
    private String packageScanner;

    /**
     * This method differentiates RollingFileAppender from its super class.
     *
     * @param event
     */
    @Override
    protected void subAppend(Object event) {
        try {
            if (event instanceof LoggingEvent) {
                super.subAppend(DesensitizeUtils.loggingEventOperation((LoggingEvent) event, packageScanner));
            }
        } catch (Exception e) {
            log.error("log error: {}", e.getMessage());
            super.subAppend(event);
        }
    }
}

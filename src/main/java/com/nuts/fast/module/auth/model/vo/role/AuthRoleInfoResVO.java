package com.nuts.fast.module.auth.model.vo.role;

import com.nuts.fast.module.auth.model.dto.role.AuthRoleUserDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 权限管理 - 角色详情信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthRoleInfoResVO", description = "权限管理 - 角色详情信息返回 VO")
public class AuthRoleInfoResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 角色编号
     */
    @Schema(name = "roleCode", description = "角色编号")
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(name = "roleName", description = "角色名称")
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 菜单权限主键列表
     */
    @Schema(name = "menuPermIds", description = "菜单权限主键列表")
    private List<Long> menuPermIds;

    /**
     * 操作权限主键列表
     */
    @Schema(name = "actionPermIds", description = "操作权限主键列表")
    private List<Long> actionPermIds;

    /**
     * 角色下用户列表
     */
    @Schema(name = "users", description = "角色下用户列表")
    private List<AuthRoleUserDTO> users;
}

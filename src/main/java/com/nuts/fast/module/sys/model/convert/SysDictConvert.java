package com.nuts.fast.module.sys.model.convert;

import com.nuts.fast.module.sys.model.entity.SysDictDO;
import com.nuts.fast.module.sys.model.vo.dict.SysDictSaveReqVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

/**
 * 系统字典信息 POJO 转换工具
 *
 * @author Nuts
 */
@Mapper(componentModel = "spring")
public interface SysDictConvert {

    SysDictConvert INSTANCE = Mappers.getMapper(SysDictConvert.class);

    /**
     * 系统字典前端保存请求VO转换系统字典信息实体类工具
     *
     * @param sysDictSaveReqVO 系统字典前端保存请求VO
     * @return 系统字典信息实体类
     */
    SysDictDO saveReqVoConvert2Do(SysDictSaveReqVO sysDictSaveReqVO);

    /**
     * 系统字典前端保存请求VO转换系统字典信息实体类工具(修改使用，仅包含部分字段)
     *
     * @param sysDictDO        系统字典信息实体类
     * @param sysDictSaveReqVO 系统字典前端保存请求VO
     * @return 系统字典信息实体类
     */
    @Mappings({
            @Mapping(source = "sysDictSaveReqVO.id", target = "id"),
            @Mapping(source = "sysDictSaveReqVO.dictName", target = "dictName"),
            @Mapping(source = "sysDictSaveReqVO.dictDesc", target = "dictDesc"),
            @Mapping(source = "sysDictSaveReqVO.status", target = "status"),
            @Mapping(source = "sysDictSaveReqVO.remark", target = "remark")
    })
    SysDictDO saveReqVoConvert2Do(SysDictDO sysDictDO, SysDictSaveReqVO sysDictSaveReqVO);
}

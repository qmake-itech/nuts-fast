package com.nuts.fast.module.auth.service.intf;

import com.nuts.fast.module.auth.model.entity.AuthPermDO;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 权限管理 - 权限信息服务接口类
 *
 * @author Nuts
 */
public interface IAuthPermService {

    /**
     * 根据角色 id 列表获取角色对应权限集合(包括菜单)
     *
     * @param roleIds 角色 id 集合
     * @return 权限集合
     */
    Optional<List<AuthPermDO>> getMenuPermSetByRoleIds(Set<Long> roleIds);

    /**
     * 根据角色 id 列表获取角色对应权限集合(操作)
     *
     * @param roleIds 角色 id 集合
     * @return 权限集合
     */
    Optional<List<AuthPermDO>> getActionPermSetByRoleIds(Set<Long> roleIds);

    /**
     * 根据角色 id 获取角色对应权限列表(包括菜单、操作)
     *
     * @param roleId 角色 id
     * @return 权限集合
     */
    List<Long> getMenuPermListByRoleId(Long roleId);

    /**
     * 根据角色 id 获取角色对应权限列表(操作)
     *
     * @param roleId 角色 id
     * @return 权限集合
     */
    List<Long> getActionPermListByRoleId(Long roleId);
}

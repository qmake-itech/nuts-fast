package com.nuts.fast.module.auth.model.vo.authorize;

import cn.hutool.core.lang.tree.Tree;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
public class CurrentUserVO implements Serializable {

    private String username;

    private String nickname;

    private String avatar;

    private String mobile;

    private String email;

    private List<Tree<Long>> menuTree;

    private String dictCacheMd5;

    private String paramCacheMd5;
}

package com.nuts.fast.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.sys.model.vo.notice.*;
import com.nuts.fast.module.sys.service.intf.ISysNoticeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 通知公告信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/sys/notice")
@Tag(name = "通知公告信息服务接口", description = "通知公告信息服务接口")
public class SysNoticeController {

    @Resource
    private ISysNoticeService sysNoticeService;

    /**
     * 获取通知公告分页列表
     *
     * @param pageNotices 分页查询请求
     * @return 通知公告分页列表
     */
    @PostMapping("/list/page")
    @Operation(summary = "获取通知公告分页列表", description = "获取通知公告分页列表")
    public IPage<SysNoticePageResVO> getSysNoticeListByPage(@RequestBody PageModel<SysNoticePageReqVO> pageNotices) {
        return sysNoticeService.page(pageNotices);
    }


    /**
     * 获取通知公告详情信息
     *
     * @param id 通知公告 Id
     * @return 通知公告详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取通知公告详情信息", description = "获取通知公告详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "通知公告主键"))
    public SysNoticeInfoResVO getSysNoticeInfo(@Valid @NotNull Long id) {
        return sysNoticeService.getInfoById(id);
    }

    /**
     * 保存通知公告信息
     *
     * @param sysNoticeSaveReq 通知公告保存请求信息
     * @return 通知公告主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存通知公告信息", description = "保存通知公告信息")
    public Long saveSysNotice(@Valid @RequestBody SysNoticeSaveReqVO sysNoticeSaveReq) {
        return sysNoticeService.saveOrUpdate(sysNoticeSaveReq);
    }

    /**
     * 根据通知公告主键删除通知公告信息
     *
     * @param id 通知公告主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据通知公告主键删除通知公告信息", description = "根据通知公告主键删除通知公告信息")
    @Parameters(@Parameter(name = "id", required = true, description = "通知公告主键"))
    public void deleteSysNoticeById(@Valid @NotNull Long id) {
        sysNoticeService.removeById(id);
    }

    /**
     * 根据通知公告主键列表批量删除通知公告信息
     *
     * @param ids 通知公告主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据通知公告主键列表批量删除通知公告信息", description = "根据通知公告主键列表批量删除通知公告信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "通知公告主键列表"))
    public void deleteSysNoticeById(@RequestBodyBySingleParam List<Long> ids) {
        sysNoticeService.removeByIds(ids);
    }

    /**
     * 根据通知公告主键推送通知公告信息
     *
     * @param id 通知公告主键
     */
    @PostMapping("/send")
    @Operation(summary = "根据通知公告主键推送通知公告信息", description = "根据通知公告主键推送通知公告信息")
    @Parameters(@Parameter(name = "id", required = true, description = "通知公告主键"))
    public void sendNotice(Long id) {
        sysNoticeService.sendNotice(List.of(id));
    }

    /**
     * 根据通知公告主键列表批量推送通知公告信息
     *
     * @param ids 通知公告主键列表
     */
    @PostMapping("/send/batch")
    @Operation(summary = "根据通知公告主键列表批量推送通知公告信息", description = "根据通知公告主键列表批量推送通知公告信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "通知公告主键列表"))
    public void sendNoticeBatch(@RequestBodyBySingleParam List<Long> ids) {
        sysNoticeService.sendNotice(ids);
    }

    /**
     * 根据当前登录的用户获取当前用户的所有通知信息列表
     *
     * @return 通知信息列表
     */
    @GetMapping("/current/list")
    @Operation(summary = "根据通知公告主键列表批量推送通知公告信息", description = "根据通知公告主键列表批量推送通知公告信息")
    public List<CurrentUserNoticeVO> getCurrentUserNotices() {
        return sysNoticeService.getCurrentUserNotices();
    }

    /**
     * 根据通知公告主键已读通知公告信息
     *
     * @param id 通知公告主键
     */
    @PostMapping("/read")
    @Operation(summary = "根据通知公告主键已读通知公告信息", description = "根据通知公告主键已读通知公告信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "通知公告主键"))
    public void readNotice(Long id) {
        sysNoticeService.readNotice(id);
    }

    /**
     * 全部已读
     */
    @PostMapping("/read/all")
    @Operation(summary = "全部已读", description = "全部已读")
    public void readNotice() {
        sysNoticeService.readAllNotice();
    }
}

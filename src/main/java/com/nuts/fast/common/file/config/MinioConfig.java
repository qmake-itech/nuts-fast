package com.nuts.fast.common.file.config;

import io.minio.MinioClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Minio 配置类
 *
 * @author nuts
 */
@Configuration
public class MinioConfig {

    @Resource
    private FileProperties fileProperties;

    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder().endpoint(fileProperties.getMinio().getUrl())
                .credentials(fileProperties.getMinio().getAccessKey(), fileProperties.getMinio().getSecretKey())
                .build();
    }
}

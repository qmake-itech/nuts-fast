package com.nuts.fast.module.sys.model.vo.dict;

import com.nuts.fast.module.sys.model.entity.SysDictItemDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 系统管理 - 字典详情信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictInfoResVO", description = "系统管理 - 字典详情信息返回 VO")
public class SysDictInfoResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 字典名称
     */
    @Schema(name = "dictName", description = "字典名称")
    private String dictName;

    /**
     * 字典描述
     */
    @Schema(name = "dictDesc", description = "字典描述")
    private String dictDesc;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 字典项信息列表
     */
    @Schema(name = "dictItems", description = "字典项信息列表")
    private List<SysDictItemDO> dictItems;
}

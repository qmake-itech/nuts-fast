package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 权限管理 - 操作信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthActionDO", description = "权限管理 - 操作信息实体类, 前端操作信息, 协助前端操作(按钮等)进行权限控制")
@TableName("auth_action")
public class AuthActionDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 菜单主键
     */
    @Schema(name = "mid", description = "菜单主键")
    private Long mid;

    /**
     * 操作编号
     */
    @Schema(name = "actionCode", description = "操作编号")
    private String actionCode;

    /**
     * 操作名称
     */
    @Schema(name = "actionName", description = "操作名称")
    private String actionName;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;
}

package com.nuts.fast.controller.sys;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.sys.model.vo.param.*;
import com.nuts.fast.module.sys.service.intf.ISysParamService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 系统参数信息服务接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/sys/param")
@Tag(name = "系统参数信息服务接口", description = "系统参数信息服务接口")
public class SysParamController {

    @Resource
    private ISysParamService sysParamService;

    /**
     * 获取系统参数分页列表
     *
     * @param pageParams 分页查询请求
     * @return 系统参数分页列表
     */
    @PostMapping("/list/page")
    @Operation(summary = "获取系统参数分页列表", description = "获取系统参数分页列表")
    public IPage<SysParamPageResVO> getSysParamListByPage(@RequestBody PageModel<SysParamPageReqVO> pageParams) {
        return sysParamService.page(pageParams);
    }


    /**
     * 获取系统参数详情信息
     *
     * @param id 系统参数 Id
     * @return 系统参数详情信息
     */
    @GetMapping("/info")
    @Operation(summary = "获取系统参数详情信息", description = "获取系统参数详情信息")
    @Parameters(@Parameter(name = "id", required = true, description = "系统参数主键"))
    public SysParamInfoResVO getSysParamInfo(@Valid @NotNull Long id) {
        return sysParamService.getInfoById(id);
    }

    /**
     * 获取所有生效的参数
     *
     * @return 参数列表
     */
    @GetMapping("/all")
    @Operation(summary = "获取所有生效的字典下字典项", description = "获取所有生效的字典下字典项")
    public SysParamMapResVO getParamMap() {
        return SysParamMapResVO.builder().paramMap(sysParamService.getParamMap()).build();
    }

    /**
     * 保存系统参数信息
     *
     * @param sysParamSaveReq 系统参数保存请求信息
     * @return 系统参数主键
     */
    @PostMapping("/save")
    @Operation(summary = "保存系统参数信息", description = "保存系统参数信息")
    public Long saveSysParam(@Valid @RequestBody SysParamSaveReqVO sysParamSaveReq) {
        return sysParamService.saveOrUpdate(sysParamSaveReq);
    }

    /**
     * 根据系统参数主键删除系统参数信息
     *
     * @param id 系统参数主键
     */
    @PostMapping("/delete")
    @Operation(summary = "根据系统参数主键删除系统参数信息", description = "根据系统参数主键删除系统参数信息")
    @Parameters(@Parameter(name = "id", required = true, description = "系统参数主键"))
    public void deleteSysParamById(@Valid @NotNull Long id) {
        sysParamService.removeById(id);
    }

    /**
     * 根据系统参数主键列表批量删除系统参数信息
     *
     * @param ids 系统参数主键列表
     */
    @PostMapping("/delete/batch")
    @Operation(summary = "根据系统参数主键列表批量删除系统参数信息", description = "根据系统参数主键列表批量删除系统参数信息")
    @Parameters(@Parameter(name = "ids", required = true, description = "系统参数主键列表"))
    public void deleteSysParamById(@RequestBodyBySingleParam List<Long> ids) {
        sysParamService.removeByIds(ids);
    }

    /**
     * 从数据库中加载缓存
     */
    @PostMapping("/refresh")
    @Operation(summary = "刷新缓存", description = "刷新缓存")
    public void refreshSysParamCache() {
        sysParamService.updateParamMapCache();
    }
}

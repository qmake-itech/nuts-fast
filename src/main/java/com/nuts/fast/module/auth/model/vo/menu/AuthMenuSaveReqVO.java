package com.nuts.fast.module.auth.model.vo.menu;

import com.nuts.fast.module.auth.model.entity.AuthActionDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 权限管理 - 菜单信息保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthMenuSaveReqVO", description = "权限管理 - 菜单信息保存请求 VO")
public class AuthMenuSaveReqVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 父菜单主键
     */
    @Schema(name = "pid", description = "父菜单主键", requiredMode = Schema.RequiredMode.REQUIRED)
    private Long pid;

    /**
     * 菜单编号
     */
    @Schema(name = "menuCode", description = "菜单编号", requiredMode = Schema.RequiredMode.REQUIRED)
    private String menuCode;

    /**
     * 菜单名称
     */
    @Schema(name = "menuName", description = "菜单名称", requiredMode = Schema.RequiredMode.REQUIRED)
    private String menuName;

    /**
     * 菜单图标
     */
    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    /**
     * 访问方法
     */
    @Schema(name = "scheme", description = "访问方法", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer scheme;

    /**
     * 访问方式
     */
    @Schema(name = "target", description = "访问方式", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer target;

    /**
     * 组件路径
     */
    @Schema(name = "path", description = "组件路径", requiredMode = Schema.RequiredMode.REQUIRED)
    private String path;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "是否在菜单中可见", requiredMode = Schema.RequiredMode.REQUIRED)
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "是否在面包屑中显示", requiredMode = Schema.RequiredMode.REQUIRED)
    private String showInBread;

    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer sort;

    /**
     * 组件是否缓存
     */
    @Schema(name = "keepLive", description = "组件是否缓存", requiredMode = Schema.RequiredMode.REQUIRED)
    private String keepLive;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态", requiredMode = Schema.RequiredMode.REQUIRED)
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 新增操作列表
     */
    @Schema(name = "insertActions", description = "新增操作列表")
    private List<AuthActionDO> insertActions;

    /**
     * 更新操作列表
     */
    @Schema(name = "updateActions", description = "操作列表")
    private List<AuthActionDO> updateActions;

    /**
     * 操作列表
     */
    @Schema(name = "removeActions", description = "操作列表")
    private List<AuthActionDO> removeActions;
}

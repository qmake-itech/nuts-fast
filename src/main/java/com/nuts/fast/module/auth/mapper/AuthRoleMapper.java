package com.nuts.fast.module.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.module.auth.model.dto.user.AuthUserRoleDTO;
import com.nuts.fast.module.auth.model.entity.AuthRoleDO;
import com.nuts.fast.module.auth.model.vo.role.AuthRoleInfoResVO;
import com.nuts.fast.module.auth.model.vo.role.AuthRolePageReqVO;
import com.nuts.fast.module.auth.model.vo.role.AuthRolePageResVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 权限管理 - 角色信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface AuthRoleMapper extends BaseMapper<AuthRoleDO> {

    /**
     * 获取角色分页信息
     *
     * @param page            分页查询
     * @param authRolePageReq 角色分页信息请求体
     * @return 角色分页信息
     */
    IPage<AuthRolePageResVO> getAuthRoleListByPage(Page<AuthRolePageReqVO> page, @Param("authRolePageReq") AuthRolePageReqVO authRolePageReq);

    /**
     * 根据角色主键获取角色详情信息
     *
     * @param id 角色主键
     * @return 角色详情信息
     */
    AuthRoleInfoResVO getAuthRoleInfoById(Long id);

    /**
     * 根据用户主键列表获取角色信息列表
     *
     * @param uid 用户主键
     * @return 角色信息列表
     */
    List<AuthUserRoleDTO> getAuthRoleListByUserId(Long uid);
}

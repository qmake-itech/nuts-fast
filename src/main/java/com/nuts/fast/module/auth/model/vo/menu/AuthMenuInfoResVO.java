package com.nuts.fast.module.auth.model.vo.menu;

import com.nuts.fast.module.auth.model.entity.AuthActionDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * 权限管理 - 菜单详情信息返回 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthMenuInfoResVO", description = "权限管理 - 菜单详情信息返回 VO")
public class AuthMenuInfoResVO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 父菜单主键
     */
    @Schema(name = "pid", description = "父菜单主键")
    private Long pid;

    /**
     * 菜单编号
     */
    @Schema(name = "menuCode", description = "菜单编号")
    private String menuCode;

    /**
     * 菜单名称
     */
    @Schema(name = "menuName", description = "菜单名称")
    private String menuName;

    /**
     * 菜单图标
     */
    @Schema(name = "icon", description = "菜单图标")
    private String icon;

    /**
     * 访问方法
     */
    @Schema(name = "scheme", description = "访问方法")
    private Integer scheme;

    /**
     * 访问方式
     */
    @Schema(name = "target", description = "访问方式")
    private Integer target;

    /**
     * 组件路径
     */
    @Schema(name = "path", description = "组件路径")
    private String path;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "是否在菜单中可见")
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "是否在面包屑中显示")
    private String showInBread;

    /**
     * 排序
     */
    @Schema(name = "sort", description = "排序")
    private Integer sort;

    /**
     * 组件是否缓存
     */
    @Schema(name = "keepLive", description = "组件是否缓存")
    private String keepLive;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 操作列表
     */
    @Schema(name = "actions", description = "操作列表")
    private List<AuthActionDO> actions;
}

package com.nuts.fast.common.web.advice;

import com.nuts.fast.common.core.exception.BusinessException;
import com.nuts.fast.common.core.exception.SystemException;
import com.nuts.fast.common.core.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一拦截 Controller 异常
 *
 * @author Nuts
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.nuts.fast")
public class ExceptionAdvice {

    /**
     * 捕获 {@code BusinessException} 异常
     */
    @ExceptionHandler({BusinessException.class})
    public R<?> handleBusinessException(BusinessException ex) {
        log.debug(ex.getMessage());
        return R.fail(ex.getMessage());
    }

    /**
     * 捕获 {@code SystemException} 异常
     */
    @ExceptionHandler({SystemException.class})
    public R<?> handleForbiddenException(SystemException ex) {
        log.info(ex.getMessage());
        return R.fail(ex.getMessage());
//        return R.fail(ResultEnum.FORBIDDEN);
    }

    /**
     * 顶级异常捕获并统一处理，当其他异常无法处理时候选择使用
     */
    @ExceptionHandler({Exception.class})
    public R<?> handle(Exception ex) {
        log.error(ex.getMessage());
        ex.printStackTrace();
        return R.fail(ex.getMessage());
    }
}

package com.nuts.fast.module.sys.service.intf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.sys.model.entity.SysDictDO;
import com.nuts.fast.module.sys.model.vo.dict.*;

import java.util.List;
import java.util.Map;

/**
 * 系统管理 - 字典信息服务接口类
 *
 * @author Nuts
 */
public interface ISysDictService extends IService<SysDictDO> {

    /**
     * 获取字典列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 字典列表分页信息
     */
    IPage<SysDictPageResVO> page(PageModel<SysDictPageReqVO> pageParams);

    /**
     * 通过字典主键获取字典详情信息
     *
     * @param id 字典主键
     * @return 字典详情信息
     */
    SysDictInfoResVO getInfoById(Long id);

    /**
     * 保存字典信息
     *
     * @param sysDictSaveReq 字典保存请求信息
     * @return 字典主键
     */
    Long saveOrUpdate(SysDictSaveReqVO sysDictSaveReq);

    /**
     * 从数据库中加载字典缓存
     *
     * @return 字典缓存
     */
    Map<String, List<SysDictItemResVO>> updateDictMapCache();

    /**
     * 获取所有生效的字典下字典项
     *
     * @return 字典及字典项列表
     */
    Map<String, List<SysDictItemResVO>> getDictWithDictItemList();
}

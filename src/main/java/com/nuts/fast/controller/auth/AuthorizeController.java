package com.nuts.fast.controller.auth;

import com.nuts.fast.common.core.exception.BusinessException;
import com.nuts.fast.common.core.exception.SystemException;
import com.nuts.fast.common.core.security.SecurityUtils;
import com.nuts.fast.common.redis.constant.RedisConstants;
import com.nuts.fast.common.web.annotation.RequestBodyBySingleParam;
import com.nuts.fast.module.auth.model.entity.AuthPermDO;
import com.nuts.fast.module.auth.service.impl.UserDetailsServiceImpl;
import com.nuts.fast.module.auth.model.vo.authorize.CurrentUserVO;
import com.nuts.fast.module.auth.model.vo.authorize.LoginReqVO;
import com.nuts.fast.module.auth.service.intf.IAuthMenuService;
import com.nuts.fast.module.auth.service.intf.IAuthPermService;
import com.nuts.fast.module.auth.service.intf.IAuthUserRoleService;
import com.nuts.fast.module.auth.service.intf.IAuthUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 权限相关接口
 *
 * @author Nuts
 */
@Slf4j
@RestController
@RequestMapping("/authorize")
@Tag(name = "权限相关接口", description = "权限相关接口")
public class AuthorizeController {

    @Resource
    private UserDetailsServiceImpl userDetailsService;

    @Resource
    private IAuthUserService authUserService;

    @Resource
    private IAuthUserRoleService authUserRoleService;

    @Resource
    private IAuthPermService authPermService;

    @Resource
    private IAuthMenuService authMenuService;

    @Resource
    private RedissonClient redissonClient;

    /**
     * 登陆并获取 token
     *
     * @param loginReq 登陆请求 VO
     * @return 登录token
     */
    @PostMapping("/login")
    @Operation(summary = "登陆并获取 token", description = "登陆并获取 token")
    public Map<String, String> login(@Valid @RequestBody LoginReqVO loginReq) {
        val token = userDetailsService.login(loginReq.getUsername(), loginReq.getPassword())
                .orElseThrow(() -> new SystemException("获取 token 失败"));
        return Map.of("token", token);
    }

    /**
     * 登出并清除 token 缓存
     *
     * @param token 登陆 token
     */
    @PostMapping("/logout")
    @Operation(summary = "登出并清除 token 缓存", description = "登出并清除 token 缓存")
    @Parameters(@Parameter(name = "token", required = true, description = "登陆 token"))
    public void logout(@Valid @NotNull @RequestBodyBySingleParam String token) {
        userDetailsService.logout(token);
    }

    /**
     * 修改当前用户密码
     *
     * @param password 新密码
     */
    @PostMapping("/change/password")
    @Operation(summary = "修改当前用户密码", description = "修改当前用户密码")
    @Parameters(@Parameter(name = "password", required = true, description = "修改当前用户密码"))
    public void changeCurrentPassword(@Valid @NotNull String password) {

        val userDetails = SecurityUtils.getUser()
                .orElseThrow(() -> new BusinessException("获取当前用户信息失败!"));

        authUserService.updatePassword(userDetails.getUserId(), password);
    }

    /**
     * 获取当前用户信息
     *
     * @return 当前用户信息
     */
    @GetMapping("/current/user")
    @Operation(summary = "获取当前用户信息", description = "获取当前用户信息")
    public CurrentUserVO getCurrentUser() {

        val userDetails = SecurityUtils.getUser()
                .orElseThrow(() -> new BusinessException("获取当前用户信息失败!"));
        log.debug("查询当前用户信息成功, 用户信息为:\r\n {}", userDetails);

        val userId = userDetails.getUserId();

        // 根据用户 id 获取最新的用户信息
        val user = authUserService.getAuthUserById(userId)
                .orElseThrow(() -> new BusinessException("获取当前用户信息失败!"));

        // 根据用户 id 获取全部角色 id 集合
        val menuTree = authUserRoleService.getRoleIdListByUserId(userId)
                .flatMap(roles -> authPermService.getMenuPermSetByRoleIds(roles))
                .map(authPerms -> authPerms.stream()
                        .filter(authPerm -> 0 == authPerm.getSubjectType())
                        .map(AuthPermDO::getSubjectId)
                        .collect(Collectors.toList()))
                .flatMap(menuIds -> authMenuService.generateMenuTreeByMenuIds(menuIds))
                .orElse(new ArrayList<>());


        RBucket<String> rBucketDict = redissonClient.getBucket(RedisConstants.DICT_MD5);
        val dictCacheMd5 = rBucketDict.get();

        RBucket<String> rBucketParam = redissonClient.getBucket(RedisConstants.PARAM_MD5);
        val paramCacheMd5 = rBucketParam.get();

        return CurrentUserVO.builder()
                .username(user.getUsername())
                .nickname(user.getNickname())
                .avatar("")
                .email(user.getEmail())
                .mobile(user.getMobile())
                .menuTree(menuTree)
                .dictCacheMd5(dictCacheMd5)
                .paramCacheMd5(paramCacheMd5)
                .build();
    }
}

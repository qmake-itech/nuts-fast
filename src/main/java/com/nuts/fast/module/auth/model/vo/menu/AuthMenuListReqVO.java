package com.nuts.fast.module.auth.model.vo.menu;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "权限管理 - 菜单信息", description = "权限管理 - 菜单信息")
public class AuthMenuListReqVO implements Serializable {

    /**
     * 菜单名称
     */
    @Schema(name = "menuName", description = "菜单名称")
    private String menuName;

    /**
     * 是否在菜单中可见
     */
    @Schema(name = "visible", description = "菜单中可见")
    private String visible;

    /**
     * 是否在面包屑中显示
     */
    @Schema(name = "showInBread", description = "面包屑中可见")
    private String showInBread;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}

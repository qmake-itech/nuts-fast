package com.nuts.fast.module.auth.model.vo.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 用户管理 - 用户分页查询请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "AuthUserPageReqVO", description = "用户管理 - 用户分页查询请求 VO")
public class AuthUserPageReqVO implements Serializable {

    /**
     * 用户名
     */
    @Schema(name = "username", description = "用户名")
    private String username;

    /**
     * 昵称
     */
    @Schema(name = "nickname", description = "昵称")
    private String nickname;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}

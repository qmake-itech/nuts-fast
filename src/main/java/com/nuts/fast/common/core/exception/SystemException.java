package com.nuts.fast.common.core.exception;

/**
 * 系统异常 (对前端显示系统异常)
 *
 * @author Nuts
 */
public class SystemException extends RuntimeException {
    public SystemException(String message) {
        super(message);
    }
}

package com.nuts.fast.module.sys.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * 系统管理 - 字典项信息实体类
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "系统管理 - 字典项信息", description = "系统管理 - 字典项信息")
@TableName("sys_dict_item")
public class SysDictItemDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 字典主键
     */
    @Schema(name = "did", description = "字典主键")
    private Long did;

    /**
     * 字典项键
     */
    @Schema(name = "dictItemKey", description = "字典项键")
    private String dictItemKey;

    /**
     * 字典项值
     */
    @Schema(name = "dictItemValue", description = "字典项值")
    private String dictItemValue;

    /**
     * Tag 类型
     */
    @Schema(name = "tagType", description = "Tag 类型")
    private String tagType;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;
}

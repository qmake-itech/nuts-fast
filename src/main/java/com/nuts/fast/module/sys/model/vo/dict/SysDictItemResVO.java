package com.nuts.fast.module.sys.model.vo.dict;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictInfoResVO", description = "系统管理 - 字典详情信息返回 VO")
public class SysDictItemResVO implements Serializable {

    /**
     * 字典项键
     */
    @Schema(name = "dictItemKey", description = "字典项键")
    private String dictItemKey;

    /**
     * 字典项值
     */
    @Schema(name = "dictItemValue", description = "字典项值")
    private String dictItemValue;

    /**
     * Tag 类型
     */
    @Schema(name = "tagType", description = "Tag 类型")
    private String tagType;
}

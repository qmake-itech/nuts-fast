package com.nuts.fast.module.sys.model.vo.dict;

import com.nuts.fast.module.sys.model.entity.SysDictItemDO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

/**
 * 系统管理 - 字典详情信息保存请求 VO
 *
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "SysDictSaveReqVO", description = "系统管理 - 字典详情信息保存请求 VO")
public class SysDictSaveReqVO {

    /**
     * 主键
     */
    @Schema(name = "id", description = "主键")
    private Long id;

    /**
     * 字典名称
     */
    @Schema(name = "dictName", description = "字典名称")
    private String dictName;

    /**
     * 字典描述
     */
    @Schema(name = "dictDesc", description = "字典描述")
    private String dictDesc;

    /**
     * 状态
     */
    @Schema(name = "status", description = "状态")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "remark", description = "备注")
    private String remark;

    /**
     * 新增字典项信息列表
     */
    @Schema(name = "insertDictItems", description = "新增字典项信息列表")
    private List<SysDictItemDO> insertDictItems;

    /**
     * 更新字典项信息列表
     */
    @Schema(name = "updateDictItems", description = "更新字典项信息列表")
    private List<SysDictItemDO> updateDictItems;

    /**
     * 删除字典项信息列表
     */
    @Schema(name = "removeDictItems", description = "删除字典项信息列表")
    private List<SysDictItemDO> removeDictItems;
}

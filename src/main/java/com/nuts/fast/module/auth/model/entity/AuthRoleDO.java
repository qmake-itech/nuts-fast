package com.nuts.fast.module.auth.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Nuts
 */
@Data
@Builder
@With
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "权限管理-角色信息", description = "权限管理-角色信息")
@TableName("auth_role")
public class AuthRoleDO implements Serializable {

    /**
     * 主键
     */
    @Schema(name = "主键", description = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 角色编号
     */
    @Schema(name = "角色编号", description = "角色编号")
    private String roleCode;

    /**
     * 角色名称
     */
    @Schema(name = "角色名称", description = "角色名称")
    private String roleName;

    /**
     * 状态
     */
    @Schema(name = "状态", description = "")
    private Integer status;

    /**
     * 备注
     */
    @Schema(name = "备注", description = "")
    private String remark;

    /**
     * 创建人
     */
    @Schema(name = "创建人", description = "")
    private Long createdBy;

    /**
     * 创建时间
     */
    @Schema(name = "创建时间", description = "")
    private Date createdTime;

    /**
     * 更新人
     */
    @Schema(name = "更新人", description = "")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @Schema(name = "更新时间", description = "")
    private Date updatedTime;
}

package com.nuts.fast.module.sys.service.intf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.nuts.fast.common.datasource.model.PageModel;
import com.nuts.fast.module.sys.model.entity.SysNoticeDO;
import com.nuts.fast.module.sys.model.vo.notice.*;

import java.util.Collection;
import java.util.List;

/**
 * 系统管理 - 通知公告信息服务接口类
 *
 * @author Nuts
 */
public interface ISysNoticeService extends IService<SysNoticeDO> {

    /**
     * 获取通知公告列表分页信息
     *
     * @param pageParams 分页查询参数
     * @return 通知公告列表分页信息
     */
    IPage<SysNoticePageResVO> page(PageModel<SysNoticePageReqVO> pageParams);

    /**
     * 通过通知公告主键获取通知公告详情信息
     *
     * @param id 通知公告主键
     * @return 通知公告详情信息
     */
    SysNoticeInfoResVO getInfoById(Long id);

    /**
     * 保存通知公告信息
     *
     * @param sysNoticeSaveReq 通知公告保存请求信息
     * @return 通知公告主键
     */
    Long saveOrUpdate(SysNoticeSaveReqVO sysNoticeSaveReq);

    /**
     * 手动推送公告
     *
     * @param ids 要推送的通知公告主键列表
     */
    void sendNotice(Collection<?> ids);

    /**
     * 根据用户主键获取公告列表
     *
     * @return 公告列表
     */
    List<CurrentUserNoticeVO> getCurrentUserNotices();

    /**
     * 根据通知公告主键已读通知公告信息
     *
     * @param id 通知公告主键
     */
    void readNotice(Long id);

    /**
     * 全部已读
     */
    void readAllNotice();
}

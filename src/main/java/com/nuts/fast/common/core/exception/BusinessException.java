package com.nuts.fast.common.core.exception;

/**
 * 业务异常
 *
 * @author Nuts
 */
public class BusinessException extends RuntimeException {
    public BusinessException(String message) {
        super(message);
    }
}

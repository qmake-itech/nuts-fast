package com.nuts.fast.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.nuts.fast.module.sys.model.dto.param.SysParamCacheDTO;
import com.nuts.fast.module.sys.model.vo.param.SysParamInfoResVO;
import com.nuts.fast.module.sys.model.vo.param.SysParamPageReqVO;
import com.nuts.fast.module.sys.model.vo.param.SysParamPageResVO;
import com.nuts.fast.module.sys.model.entity.SysParamDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统管理 - 系统参数信息数据库访问层
 *
 * @author Nuts
 */
@Mapper
public interface SysParamMapper extends BaseMapper<SysParamDO> {

    /**
     * 获取系统参数分页信息
     *
     * @param page            分页查询
     * @param sysParamPageReq 系统参数分页信息请求体
     * @return 系统参数分页信息
     */
    IPage<SysParamPageResVO> getSysParamListByPage(Page<SysParamPageResVO> page, @Param("sysParamPageReq") SysParamPageReqVO sysParamPageReq);

    /**
     * 根据系统参数主键获取系统参数详情信息
     *
     * @param id 系统参数主键
     * @return 系统参数详情信息
     */
    SysParamInfoResVO getSysParamInfoById(Long id);

    /**
     * 获取全部系统参数
     *
     * @return 系统参数
     */
    List<SysParamCacheDTO> getSysParamList();
}
